﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl.Http;
using Newtonsoft.Json;
using PersonGenerator;

namespace WebClient;

internal static class Program
{
    private const string BaseUrl = "https://localhost:5001/customers";

    private static async Task Main(string[] args)
    {
        var canExit = false;
        while (!canExit)
        {
            Console.Write(
                "1.Отобразить покупателя по идентификатору\n" +
                "2.Создать и отправить покупателя\n" +
                "3.Завершить\n");

            if (int.TryParse(Console.ReadLine(), out var data))
                switch (data)
                {
                    case 1:
                        await GetCustomer();
                        break;
                    case 2:
                        await SendCustomer();
                        break;
                    case 3:
                        canExit = true;
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда");
                        break;
                }
        }
    }

    private static CustomerCreateRequest RandomCustomer()
    {
        var settings = new GeneratorSettings
        {
            Language = Languages.English,
            FirstName = true,
            LastName = true
        };
        var personGenerator = new PersonGenerator.PersonGenerator(settings);

        return new CustomerCreateRequest(personGenerator.Generate(1).FirstOrDefault());
    }

    private static async Task GetCustomer()
    {
        Console.WriteLine("\nВведите id: ");

        if (long.TryParse(Console.ReadLine(), out var id))
        {
            var response = await BaseUrl.WithClient(new FlurlClient(
                new HttpClient(new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain,
                        errors) => true
                }))).AppendPathSegment($"/{id}").GetStringAsync();
            var customer = JsonConvert.DeserializeObject<Customer>(response);

            if (customer != null)
                Console.WriteLine("\nПолучен покупатель: \n" +
                                  $"{customer.Id}. {customer.Firstname} {customer.Lastname}\n");
        }
        else
        {
            Console.WriteLine("Неверно указан идентификатор");
        }
    }

    private static async Task SendCustomer()
    {
        var customer = RandomCustomer();

        Console.WriteLine("\nСгенерированный покупатель: \n" +
                          $"{customer?.Firstname} {customer?.Lastname}\n");

        var customerRequest = new CustomerCreateRequest(customer?.Firstname, customer?.Lastname);
        var content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8);
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        var id = await BaseUrl.WithClient(new FlurlClient(
            new HttpClient(new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain,
                    errors) => true
            }))).PostAsync(content).ReceiveJson<long>();
        Console.WriteLine($"Создан покупатель с id: {id}");
    }
}