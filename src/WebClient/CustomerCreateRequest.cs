using PersonGenerator;

namespace WebClient;

public class CustomerCreateRequest
{
    public CustomerCreateRequest()
    {
    }

    public CustomerCreateRequest(
        string firstName,
        string lastName)
    {
        Firstname = firstName;
        Lastname = lastName;
    }

    public CustomerCreateRequest(Person person)
    {
        Firstname = person?.FirstName;
        Lastname = person?.LastName;
    }

    public string Firstname { get; init; }

    public string Lastname { get; init; }
}