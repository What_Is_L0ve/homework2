using PersonGenerator;

namespace WebClient;

public class Customer
{
    public Customer()
    {
    }

    public Customer(Person person)
    {
        Firstname = person?.FirstName;
        Lastname = person?.LastName;
    }

    public long Id { get; init; }

    public string Firstname { get; init; }

    public string Lastname { get; init; }
}