﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Models;

namespace WebApi;

public static class DependencyInjection
{
    public static IServiceCollection AddWebApi(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddDbContext<CustomerContext>(options => options.UseInMemoryDatabase("CustomerDB"));

        return serviceCollection;
    }
}