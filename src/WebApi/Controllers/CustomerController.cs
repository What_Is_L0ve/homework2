using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using WebApi.Models;

namespace WebApi.Controllers;

[Route("customers")]
public class CustomerController : Controller
{
    private readonly CustomerContext _customerContext;

    public CustomerController(CustomerContext customerContext)
    {
        _customerContext = customerContext;
    }

    [HttpGet("{id:long}")]
    [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(Customer))]
    public async Task<Customer> GetCustomerAsync([FromRoute] long id)
    {
        var customers = await _customerContext.Customers.FindAsync(id);
        if (customers != null) return customers;

        throw new BadHttpRequestException("Данный пользователь не существует", StatusCodes.Status404NotFound);
    }

    [HttpPost("")]
    [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(long))]
    public async Task<long> CreateCustomerAsync([FromBody] Customer customer)
    {
        await _customerContext.Customers.AddAsync(customer);
        await _customerContext.SaveChangesAsync();
        return customer.Id;
    }
}